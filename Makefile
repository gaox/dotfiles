build: .vim/bundle/vimproc/autoload/vimproc_unix.so 

restore:
	git submodule sync
	git submodule update --init

fetch:
	git submodule foreach 'git fetch origin'

update:
	git submodule foreach 'git checkout origin/master'
	cd .vim/bundle/powerline; git checkout origin/develop

summary:
	@git submodule summary | sed 's/ </ <<<<<<<<<<<</'

.vim/bundle/vimproc/autoload/vimproc_unix.so: .vim/bundle/vimproc/autoload/proc.c
	cd .vim/bundle/vimproc; make -f make_unix.mak

clean:
	rm -f .vim/bundle/vimproc/autoload/vimproc_unix.so

install:
	rm -f ~/.bashrc
	rm -f ~/.vimrc
	rm -rf ~/.vim
	rm -f ~/.ctags
	rm -f ~/.gitconfig
	rm -f ~/.gitignore.global
	rm -f ~/.ackrc
	rm -f ~/.zshrc
	rm -f ~/.tmux.conf
	rm -f ~/.screenrc
	rm -f ~/.pentadactylrc
	rm -f ~/.ssh/config
	ln -s ~/dotfiles/.bashrc ~/.bashrc
	ln -s ~/dotfiles/.vimrc ~/.vimrc
	ln -s ~/dotfiles/.vim ~/.vim
	ln -s ~/dotfiles/.ctags ~/.ctags
	ln -s ~/dotfiles/.gitconfig ~/.gitconfig
	ln -s ~/dotfiles/.gitignore.global ~/.gitignore.global
	ln -s ~/dotfiles/.ackrc ~/.ackrc
	ln -s ~/dotfiles/.zshrc ~/.zshrc
	ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf
	ln -s ~/dotfiles/.screenrc ~/.screenrc
	ln -s ~/dotfiles/.pentadactylrc ~/.pentadactylrc
	ln -s ~/dotfiles/.pentadactyl ~/.pentadactyl
	ln -s ~/dotfiles/.ssh/config ~/.ssh/config
