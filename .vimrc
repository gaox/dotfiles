version 6.1

runtime bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect()

runtime! config/**/*.vim

"Gundo
nnoremap <F6> :GundoToggle<CR>

"settings handy ..
set scrolloff=5
set autoindent backspace=2 showmode  showmatch  showcmd wrap
set laststatus=2 ruler wrapmargin=8
set shiftwidth=2 tabstop=4 expandtab joinspaces magic report=0 dict=/usr/dict/words
set bs=2 shell=bash wildchar=<TAB> incsearch
set mouse=n
set number
set hlsearch
nmap <F2> :noh<CR>

"sets color of cursor according to the color of its text
set t_vs=[5%dm

set textwidth=2000

" I don't like bells .
set noerrorbells
set visualbell
set t_vb=

"  background ..I like a dark one ...
" set background=light
"set background=light

" color scheme
color molokai
"set background=dark
"colorscheme solarized
" tell the term has 256 colors
set t_Co=256

" echo something on leaving .......
" au VimLeave * echo "There goes another text file!"

"syntax highlighting..
syntax on

 " correct these frequent typographical errors
 ab alos also
 ab aslo also
 ab charcter character
 ab charcters characters
 ab exmaple example
 ab shoudl should
 ab seperate separate
 ab teh the

 "shortcuts...
 ab #s #include <stdio.h>
 ab #i #include <iostream>
 ab {p public static void main(String args[]){
 ab {d (do ((i 0 (+ i 1))) ((= i
"source $VIM/macros/file_select.vim
map - :se wrapmargin=0<CR>
map + :se wrapmargin=8<CR>
map K j.
map <C-^> <C-]>
map # :e#<CR>

"map <F5> :b1<CR>
"map <F6> :b2<CR>
"map <F7> :b3<CR>
"map <F8> :b4<CR>

" god file manager
"map <F9> _ls

" kick off emax
map <F10> :!emacs -nw %<CR>:e!<CR><CR>

" exeute current line
" map <F10> "hyy@h

cnoremap <C-A> <Home>
cnoremap <C-F> <Right>
cnoremap <C-B> <Left>
cnoremap <ESC>b <S-Left>
cnoremap <ESC>f <S-Right>
cnoremap <ESC><C-H> <C-W>

"-------------> Edit compressed files <-------------
:autocmd! BufReadPre,FileReadPre  	*.gz set bin
:autocmd  BufReadPost,FileReadPost	*.gz '[,']!gunzip
:autocmd  BufReadPost,FileReadPost	*.gz set nobin
:autocmd! BufWritePost,FileWritePost	*.gz !mv <afile> <afile>:r
:autocmd  BufWritePost,FileWritePost	*.gz !gzip <afile>:r
:autocmd! FileAppendPre			*.gz !gunzip <afile>
:autocmd  FileAppendPre			*.gz !mv <afile>:r <afile>
:autocmd! FileAppendPost		*.gz !mv <afile> <afile>:r
:autocmd  FileAppendPost		*.gz !gzip <afile>:r

"-------------> Perl programs <-------------
:autocmd BufEnter  *.pl		set ai si cin formatoptions=croql
:autocmd BufLeave  *.pl		set nosi nocin formatoptions=tcq
:autocmd BufEnter  *.pl		set comments=b:#
:autocmd BufLeave  *.pl		set comments=sr:/*,mb:*,el:*/,://,b:#,:%,:XCOMM,n:>,fb:-

"-------------> C programs and header files <-------------
:autocmd BufEnter *.ll setf lex
:autocmd BufEnter *.java,*.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r set ai si cin formatoptions=crql
:autocmd BufLeave *.java,*.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r set nosi nocin formatoptions=tcq
:autocmd BufEnter *.java,*.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r set comments=sr:/*,mb:*,el:*/,://
:autocmd BufLeave *.java,*.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r set comments=sr:/*,mb:*,el:*/,://,b:#,:%,:XCOMM,n:>,fb:-
":autocmd BufEnter *.java,*.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r map <F1> :!man -a <cword><CR><CR>
":autocmd BufEnter *.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r map <F2> mpI/* <ESC>A */<ESC>`pj
":autocmd BufEnter *.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r map <F3> mp^3x$xxx`pj
":autocmd BufEnter *.java                   map <F2> mpI//<ESC>`pj
":autocmd BufEnter *.java                   map <F3> mp^2x`pj
":autocmd BufEnter *.java,*.l,*.ll,*.scm,*.y,*.yy,?akefile*,*.c,*.cc,*.h,*.r map <F4> :!ctags -t *.{c,cc,h}<CR>
":autocmd BufLeave *.java,*.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r unmap <F1>
":autocmd BufLeave *.java,*.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r unmap <F2>
":autocmd BufLeave *.java,*.l,*.ll,*.scm,*.y,*.yy,*.c,*.cc,*.h,*.r unmap <F3>
":autocmd BufLeave *.java,*.l,*.ll,*.scm,*.y,*.yy,?akefile*,*.c,*.cc,*.h,*.r unmap <F4>

"-------------> TeX & LaTeX source files <-------------
:autocmd BufEnter *.tex		map! <F1> }
:autocmd BufEnter *.tex		map! <F2> {\bf 
:autocmd BufEnter *.tex		map! <F3> {\em 
:autocmd BufEnter *.tex		map! <F4> {\

"-------------> Haskell source files <-------------
:autocmd BufEnter *.gs		map <F1> :w<CR>:!gofer %<CR>
:autocmd BufLeave *.gs		unmap <F1>

:autocmd BufEnter *.tex		ab hp hyperplane
:autocmd BufEnter *.tex		ab hps hyperplanes
:autocmd BufEnter *.tex		ab hc hypercube
:autocmd BufEnter *.tex		ab hcs hypercubes
:autocmd BufEnter *.tex		ab nd n-dimensional
:autocmd BufEnter *.tex		ab kd k-dimensional
:autocmd BufEnter *.tex		ab gc graycode

"-------------> Html Files <-------------------------
  nmap  ,e :e ~/.P/
  map   ,rn :0r ~/.P/txt/New.page.form.html
  nmap  ,p :!chmod 644 %<CR>
  ab Ybr <br>
  ab Yhr <hr>
  ab Yp  <p>
  map ,me yiwi<<ESC>ea></<C-R>"><ESC>
  ab  Ycom  <!--X--><ESC>FXs
  vmap ,com v`<i<!--<ESC>`>i--><ESC>
  ab  Ybl  <blockquote></blockquote><ESC>T>i
  ab  Yb   <b>i</b><ESC>T>
  vmap ,b   "zdi<b><C-R>z</b><ESC>2F>
  ab  Ycen <center></center><ESC>T>i
  vmap ,cen "zdi<center><C-M><C-R>z<C-M></center><ESC>T>i
  ab  Ycod <code></code><ESC>T>i
  vmap ,cod "zdi<code><C-M><C-R>z<C-M></code><C-M><ESC>T>i
  ab  Yi   <i></i><ESC>T>i
  vmap ,i   "zdi<i><C-R>z</i><ESC>T>
  ab  Ytt   <tt></tt><ESC>T>i
  vmap ,tt   "zdi<tt><C-R>z</tt><ESC>T>
  ab  Ypre <pre></pre><ESC>T>i
  vmap ,pre mz:<ESC>'<O<pre><ESC>'>o</pre><ESC>`z
  ab  Yxmp <xmp></xmp><ESC>T>i
  vmap ,xmp mz:<ESC>'<O<xmp><ESC>'>o</xmp><ESC>`z
  ab  Ytd  <td></td><ESC>T>i
  vmap ,td  "zdi<td><C-R>z</td><ESC>T>i
  ab  Ytr  <tr></tr><ESC>T>i
  vmap ,tr  "zdi<tr><C-R>z</tr><ESC>T>i
  ab  Yh1 <h1></h1><ESC>T>i
  vmap ,h1 "zdi<h1><C-R>z</h1><ESC>2F>
  ab  Yh2 <h2></h2><ESC>T>i
  vmap ,h2 "zdi<h2><C-R>z</h2><ESC>2F>
  ab  Yh3 <h3></h3><ESC>T>i
  vmap ,h3 "zdi<h3><C-R>z</h3><ESC>2F>
  ab  Yh4 <h4></h4><ESC>T>i
  vmap ,h4 "zdi<h4><C-R>z</h4><ESC>2F>
  ab  Yh5 <h5></h5><ESC>T>i
  vmap ,h5 "zdi<h5><C-R>z</h5><ESC>2F>
  ab  Yh6 <h6></h6><ESC>T>i
  vmap ,h6 "zdi<h6><C-R>z</h6><ESC>2F>
  ab Yol <ol><CR><li><CR></ol><ESC>k
  ab Yul <ul><CR><li><CR></ul><ESC>k
  ab Ydl <dl><CR><CR><dt><CR><dd><CR><p><CR><CR></dl><CR><ESC>5kA
  ab Yli <li>
  ab Ydt <dt><CR><dd><CR><p><CR><ESC>kA
  ab Ydp <dt><CR><dd><C-M><p><C-M><ESC>kkkA
  ab  Yhref <a href=""></a><ESC>?""<CR>a
  vmap ,href "zdi<a href=""><C-R>z</a><ESC>F"i
  ab  Ylink <a href=""></a><ESC>?""<CR>a
  vmap ,link "zdi<a href="<C-R>z"<C-M><C-I>><C-R>z</a><ESC>F"i
  ab  Yname <a name=""></a><ESC>?""<CR>a
  vmap ,name "zdi<a name="<C-R>z"<C-M><C-I>><C-R>z</a><ESC>2F>
  ab  Yimg  <img alt="[]"<C-M>   align=<C-M>     src=""></a><ESC>?""<CR>a
  ab  Ymail <a href="mailto:"></a><ESC>?:<CR>a
  vmap ,mail "zdi<a href="mailto:<C-R>z"<C-M><C-I><C-I>><C-R>z</a><ESC>2F>
  vmap ,Mail "zdi<a href="mailto:<C-R>z"><C-R>z</a><ESC>2F>
  ab  Ynews <a href="news:"></a><ESC>?:<CR>a
  vmap ,news "zdi<a href="news:<C-R>z"><C-R>z</a><ESC>2F>
  ab  Ypage   <C-M>page:<C-I><C-M>link:<C-I><C-M>text:<C-I><ESC>kkA
  vmap ,Cblu "zdi<FONT COLOR="#0000FF"><C-R>z</FONT>
  vmap ,Cgre "zdi<FONT COLOR="#00FF00"><C-R>z</FONT>
  vmap ,Cred "zdi<FONT COLOR="#FF0000"><C-R>z</FONT>
  imap ;& &amp;
  imap ;K &copy;
  imap ;" &quot;
  imap ;< &lt;
  imap ;> &gt;
  imap \Ae &Auml;
  imap \Oe &Ouml;
  imap \Ue &Uuml;
  imap \ae &auml;
  imap \oe &ouml;
  imap \ue &uuml;
  imap \ss &szlig;
  nmap  ,= :%s/^===\(.*\)$/<h1>\1<\/h1>/c<CR>
  ab  Ycut  \| <a href="#"<C-I>></a><ESC>F#a
  vmap ,cut  "zdi<a href="#<C-R>z"<C-I>><C-R>z</a><ESC>2F>

"Class Files
augr class
au!
au bufreadpost,filereadpost *.class %!jad -noctor -ff -i -p %
au bufreadpost,filereadpost *.class set readonly
au bufreadpost,filereadpost *.class set ft=java
au bufreadpost,filereadpost *.class normal gg=G
au bufreadpost,filereadpost *.class set nomodified 
augr END

" Latex stuff
" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin on

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
set shellslash

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: This enables automatic indentation as you type.
filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

if exists('+colorcolumn')
  set colorcolumn=80
endif

" Show the line of the cursor
set cursorline

" Use Ctrl-S to save file
map <C-s> :w<CR>
imap <C-s> <ESC>:w<CR>a

" automatically move to the next line when the cursor goes to the end of current line
set whichwrap=b,s,<,>,[,]

" delete the tab by only pressing backspace once
set smarttab

" Use Ctrl-JKHL to switch among windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Matching parenthesis, brackets, braces
"inoremap ( ()<ESC>i
"inoremap ) <c-r>=ClosePair(')')<CR>
"inoremap { {}<ESC>i
"inoremap } <c-r>=ClosePair('}')<CR>
"inoremap [ []<ESC>i
"inoremap ] <c-r>=ClosePair(']')<CR>

"function ClosePair(char)
  "if getline('.')[col('.') - 1] == a:char
    "return ""
  "else
    "return a:char
  "endif
"endf

" configure tags - add additional tags here or comment out not-used ones
set tags+=~/.vim/tags/cpp

" build tags of your own project with F10
map <F10> :!ctags -R --sort=yes --c++-kinds=+pl --fields=+iaS --extra=+q .<CR>

" OmniCppComplete
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 1 " autocomplete after .
let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
" automatically open and close the popup menu / preview window
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview

" Open/Close taglist
map <silent> tl :TlistToggle<CR>

" Nerdtree 
" map <silent> nd :NERDTreeToggle<CR>

" Nerdtree Tab 
map <silent> nd :NERDTreeTabsToggle<CR>

" Fold
set foldenable

" Single Compile
nmap <F8> :SCCompile<cr>
nmap <F9> :SCCompileRun<cr>

" Tabularize
let mapleader=';'
if exists(":Tabularize")
  nmap <Leader>a= :Tabularize /=<CR>
  vmap <Leader>a= :Tabularize /=<CR>
  nmap <Leader>a: :Tabularize /:\zs<CR>
  vmap <Leader>a: :Tabularize /:\zs<CR>
endif
inoremap <silent> <Bar>   <Bar><Esc>:call <SID>align()<CR>a
 
function! s:align()
  let p = '^\s*|\s.*\s|\s*$'
  if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
    let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
    let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
    Tabularize/|/l1
    normal! 0
    call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
  endif
endfunction

" Tagbar
map <silent> tt :TagbarToggle<CR>

" VimShell
map <silent> <Leader>sh :VimShellPop<CR>

" Session
map <silent> <Leader>os :OpenSession<CR>
map <silent> <Leader>so :CloseSession<CR>

" Tagbar xquery
let g:tagbar_type_xquery = {
    \ 'ctagstype' : 'xquery',
    \ 'kinds'     : [
        \ 'f:function',
        \ 'v:variable',
        \ 'm:module',
    \ ]
\ }

" Ctrl P buffer
map <silent> <Leader>b :CtrlPBuffer<CR>

" Show tab number
set tabline=%!SetTabLine()

" move the cursor to the end of the block after yanking
vnoremap Y y`>

" copy paste to outside 
vmap i "+y
nmap m "+p

" remap escape
imap ;; <Esc>

" Select all
nmap <C-a> gg<S-v>G
imap <C-a> <Esc>gg<S-v>G

" auto save
au FocusLost * silent! wa
set autowriteall

" Zencoding
let g:user_zen_settings = {
\  'php' : {
\    'extends' : 'html',
\    'filters' : 'c',
\  },
\  'xml' : {
\    'extends' : 'html',
\  },
\  'haml' : {
\    'extends' : 'html',
\  },
\}

" Choose which function to jump if there are multiple functions with same name
noremap <C-]> g<C-]>

" syntax for glsl
au BufNewFile,BufRead *.frag,*.vert,*.fp,*.vp,*.glsl,*.glslv,*.glslf set filetype=glsl 

" For NerdTree on Mac
let g:NERDTreeDirArrows=0

set pastetoggle=<Leader>p

" Tab
function! SetTabLine()
  " NOTE: left/right padding of each tab was hard coded as 1 space.
  " NOTE: require Vim 7.3 strwidth() to display fullwidth text correctly.

  " settings
  let tabMinWidth = 0
  let tabMaxWidth = 40
  let tabMinWidthResized = 15
  let tabScrollOff = 5
  let tabEllipsis = '…'
  let tabDivideEquel = 0

  let s:tabLineTabs = []

  let tabCount = tabpagenr('$')
  let tabSel = tabpagenr()

  " fill s:tabLineTabs with {n, filename, split, flag} for each tab
  for i in range(tabCount)
    let tabnr = i + 1
    let buflist = tabpagebuflist(tabnr)
    let winnr = tabpagewinnr(tabnr)
    let bufnr = buflist[winnr - 1]

    let filename = bufname(bufnr)
    let filename = fnamemodify(filename, ':p:t')
    let buftype = getbufvar(bufnr, '&buftype')
    if filename == ''
      if buftype == 'nofile'
        let filename .= '[Scratch]'
      else
        let filename .= '[New]'
      endif
    endif
    let split = ''
    let winCount = tabpagewinnr(tabnr, '$')
    if winCount > 1   " has split windows
      let split .= winCount
    endif
    let flag = ''
    if getbufvar(bufnr, '&modified')  " modified
      let flag .= '+'
    endif
    if strlen(flag) > 0 || strlen(split) > 0
      let flag .= ' '
    endif

    call add(s:tabLineTabs, {'n': tabnr, 'split': split, 'flag': flag, 'filename': filename})
  endfor

  " variables for final oupout
  let s = ''
  let l:tabLineTabs = deepcopy(s:tabLineTabs)

  " overflow adjustment
  " 1. apply min/max tabWidth option
  if s:TabLineTotalLength(l:tabLineTabs) > &columns
    unlet i
    for i in l:tabLineTabs
      let tabLength = s:CalcTabLength(i)
      if tabLength < tabMinWidth
        let i.filename .= repeat(' ', tabMinWidth - tabLength)
      elseif tabMaxWidth > 0 && tabLength > tabMaxWidth
        let reserve = tabLength - StrWidth(i.filename) + StrWidth(tabEllipsis)
        if tabMaxWidth > reserve
          let i.filename = StrCrop(i.filename, (tabMaxWidth - reserve), '~') . tabEllipsis
        endif
      endif
    endfor
  endif
  " 2. try divide each tab equal-width
  if tabDivideEquel
    if s:TabLineTotalLength(l:tabLineTabs) > &columns
      let divideWidth = max([tabMinWidth, tabMinWidthResized, &columns / tabCount, StrWidth(tabEllipsis)])
      unlet i
      for i in l:tabLineTabs
        let tabLength = s:CalcTabLength(i)
        if tabLength > divideWidth
          let i.filename = StrCrop(i.filename, divideWidth - StrWidth(tabEllipsis), '~') . tabEllipsis
        endif
      endfor
    endif
  endif
  " 3. ensure visibility of current tab 
  let rhWidth = 0
  let t = tabCount - 1
  let rhTabStart = min([tabSel - 1, tabSel - tabScrollOff])
  while t >= max([rhTabStart, 0])
    let tab = l:tabLineTabs[t]
    let tabLength = s:CalcTabLength(tab)
    let rhWidth += tabLength
    let t -= 1
  endwhile
  while rhWidth >= &columns
    let tab = l:tabLineTabs[-1]
    let tabLength = s:CalcTabLength(tab)
    let lastTabSpace = &columns - (rhWidth - tabLength)
    let rhWidth -= tabLength
    if rhWidth > &columns
      call remove(l:tabLineTabs, -1)
    else
      " add special flag (will be removed later) indicating that how many
      " columns could be used for last displayed tab.
      if tabSel <= tabScrollOff || tabSel < tabCount - tabScrollOff
        let tab.flag .= '>' . lastTabSpace
      endif
    endif
  endwhile

  " final ouput
  unlet i
  for i in l:tabLineTabs
    let tabnr = i.n

    let split = ''
    if strlen(i.split) > 0
      if tabnr == tabSel
        let split = '%#TabLineSplitNrSel#' . i.split .'%#TabLineSel#'
      else
        let split = '%#TabLineSplitNr#' . i.split .'%#TabLine#'
      endif
    endif

    let text = ' ' . split . i.flag . i.filename . ' '

    if i.n == l:tabLineTabs[-1].n
       if match(i.flag, '>\d\+') > -1 || i.n < tabCount
        let lastTabSpace = matchstr(i.flag, '>\zs\d\+')
        let i.flag = substitute(i.flag, '>\d\+', '', '')
        if lastTabSpace <= strlen(i.n)
          if lastTabSpace == 0
            let s = strpart(s, 0, strlen(s) - 1)
          endif
          let s .= '%#TabLineMore#>'
          continue
        else
          let text = ' ' . i.split . i.flag . i.filename . ' '
          let text = StrCrop(text, (lastTabSpace - strlen(i.n) - 1), '~') . '%#TabLineMore#>'
          let text = substitute(text, ' ' . i.split, ' ' . split, '')
        endif
       endif
    endif

    let s .= '%' . tabnr . 'T'  " start of tab N

    if tabnr == tabSel
      let s .= '%#TabLineNrSel#' . tabnr . '%#TabLineSel#'
    else
      let s .= '%#TabLineNr#' . tabnr . '%#TabLine#'
    endif

    let s .= text

  endfor

  let s .= '%#TabLineFill#%T'
  return s
endf

function! s:CalcTabLength(tab)
  return strlen(a:tab.n) + 2 + strlen(a:tab.split) + strlen(a:tab.flag) + StrWidth(a:tab.filename)
endf

function! s:TabLineTotalLength(dict)
  let length = 0
  for i in (a:dict)
    let length += strlen(i.n) + 2 + strlen(i.split) + strlen(i.flag) + StrWidth(i.filename)
  endfor
  return length
endf

function StrWidth(str)
  if exists('*strwidth')
    return strwidth(a:str)
  else
    let strlen = strlen(a:str)
    let mstrlen = strlen(substitute(a:str, ".", "x", "g"))
    if strlen == mstrlen
      return strlen
    else
      return strlen
    endif
  endif
endf

function! StrCrop(str, len, ...)
  let l:padChar = a:0 > 0 ? a:1 : ' '
  if exists('*strwidth')
    let text = substitute(a:str, '\%>' . a:len . 'c.*', '', '')
    let remainChars = split(substitute(a:str, text, '', ''), '\zs')
    while strwidth(text) < a:len
      let longer = len(remainChars) > 0 ? (text . remove(remainChars, 0)) : text
      if strwidth(longer) < a:len
        let text = longer
      else
        let text .= l:padChar
      endif
    endwhile
    return text
  else
    return substitute(a:str, '\%>' . a:len . 'c.*', '', '')
  endif
endf

if &filetype != 'tex'
  "imap <m-C-J> <Plug>IMAP_JumpForward
  "nmap <m-C-J> <Plug>IMAP_JumpForward
  "vmap <m-C-J> <Plug>IMAP_JumpForward
  "vmap <m-C-J> <Plug>IMAP_DeleteAndJumpForward
  imap <Leader>j <Plug>IMAP_JumpForward
  nmap <Leader>j <Plug>IMAP_JumpForward
  vmap <Leader>j <Plug>IMAP_JumpForward
  vmap <Leader>j <Plug>IMAP_DeleteAndJumpForward
endif

" Some Linux distributions set filetype in /etc/vimrc.
" Clear filetype flags before changing runtimepath to force Vim to reload them.
filetype off
filetype plugin indent off
set runtimepath+=/usr/local/go/misc/vim
filetype plugin indent on
syntax on
