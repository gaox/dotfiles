#!/usr/bin/env zsh

alias lsl='ls -lrt'
alias lsla='ls -lart'
alias v='vi'
alias ev='evince'
alias rm='rm -i'
alias mk='make'

alias vim="stty stop '' -ixoff ; vim"
alias vi="stty stop '' -ixoff ; vim"
# `Frozing' tty, so after any command terminal settings will be restored
ttyctl -f
alias gitk='gitk 2>/dev/null'
