OHMYZSH=$HOME/.zsh/oh-my-zsh

for lib ($OHMYZSH/lib/*.zsh); do
  source $lib
done

for conf ($HOME/.zsh/*.zsh); do
  source $conf
done

source $HOME/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOME/.zsh/zsh-history-substring-search/zsh-history-substring-search.zsh

source $OHMYZSH/themes/bira.zsh-theme

unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
  source /etc/zsh_command_not_found
fi

#if [[ "$unamestr" == 'Darwin' ]]; then
  #alias ctags=/usr/local/Cellar/ctags/5.8/bin/ctags
#fi

fpath=($HOME/.zsh/zsh-completions $fpath)

autoload -U compinit
compinit -i

export PATH=/home/gaox/.cabal/bin:$PATH
export GEM_HOME=$HOME/gems
export PATH=$HOME/gems/bin:$PATH
export PATH="/usr/local/bin:/usr/local/Cellar/sbt/0.12.1/bin:$PATH"

source /sw/bin/init.sh

__git_files () { 
    _wanted files expl 'local files' _files     
}

function git_prompt_info() {
ref=$(git symbolic-ref HEAD 2> /dev/null) || return
echo "$ZSH_THEME_GIT_PROMPT_PREFIX${ref#refs/heads/}$ZSH_THEME_GIT_PROMPT_SUFFIX"
}
